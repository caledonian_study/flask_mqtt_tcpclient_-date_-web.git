#!/usr/bin/python
import threading
from matplotlib.animation import FuncAnimation
import numpy as np
import matplotlib.pyplot as plt
import base64
import json
import time
from random import randrange

from flask import Flask, render_template
from matplotlib.pyplot import switch_backend

from pyecharts import options as opts
from pyecharts.charts import Bar, Line
from gevent import pywsgi

from flask import json
from flask import Flask, request, render_template_string, jsonify
from queue import Queue
import random
import sys
import os

import pyecharts.options as opts
from pyecharts.charts import Line
from pyecharts.faker import Faker
from pyecharts.commons.utils import JsCode


#print(os.getcwd())#当前工作路径
#print( os.path.abspath(os.path.join(os.getcwd(), "..")))#
#print( os.path.abspath(os.path.dirname(os.path.dirname(__file__))))
#print( os.path.abspath(os.path.dirname(os.getcwd())))
#print( os.path.abspath(os.path.join(os.getcwd(), "..\datemqtt\deviceservice")))
#print("end")
#print( os.path.abspath(os.path.join(os.getcwd(), "..")))#
#sys.path.append('d:\HaikeChallage\flask_mqtt_tcpclient_-date_-web\datemqtt\webserver\deviceservice')#导入绝对路径
sys.path.append(os.path.abspath(os.path.join(os.getcwd(), "..\datemqtt\deviceservice")))#导入相对路径到当前的工作环境

from lora0080e11500004a9c import wleLoRaThread,n
from wifi3080tcpclient import wifi3080thread,wifi3080ADC

#from deviceservice.lora0080e11500004a9c import wleLoRaThread,n
#from deviceservice.lora0080e11500004a9c import wifi3080thread,wifi3080ADC

app = Flask(__name__, static_folder="templates")
#app = Flask(__name__)


x_data = ["Mon", "Tue", "Wed", "Thu", "Fri", "Sat", "Sun"]
y_data = [820, 932, 901, 934, 1290, 1330, 1320]

x = []
y = []


@app.route("/getf735Date", methods=['POST'])
def f537_get():
    # 获取Get数据
    length = 0
    #print(request.form['page'])
    #print(n["magnetic_field.x"])
    #print("getf735Date")
    str=("GateADC:{}").format(random.randint(0,99))
    #print(str)
    queue.put(str.encode())
    #print("getf735Date")
    temp=[1,2,3,4,5]
    #return jsonify({"data": temp})
    #print(wifi3080ADC["wifi3080sensor"])
    return jsonify({"data": wifi3080ADC["wifi3080sensor"]})

@app.route("/beep", methods=['POST'])
def test_get():
    print("beep")
    str="BeepTest:{}".format(random.randint(1,2))
    print(str)
    queue.put(str.encode())
    temp = [1]
    return jsonify({"data": temp})

@app.route("/getDate", methods=['POST'])
def ok_get():
    # 获取Get数据
    length = 0
    length = len(n[request.form['page']])
    x = range(length)
    temp = []
    j = 0
    for i in n[request.form['page']]:
        temp.append([j, i])
        j = j+1
    return jsonify({"data": temp})

@app.route("/lineChart", methods=['POST', 'GET'])
def get_bar_chart():
    # print(request.form['page'])
    # print(request.form.get('id'))
    global date, x
    page = request.form['page']
    length = 0
    length = len(date[page])
    x = range(length)
    line = (
        Line()
        .add_xaxis(xaxis_data=x)
        .add_yaxis(
            series_name="",
            y_axis=date[page],
            is_smooth=True,
            label_opts=opts.LabelOpts(is_show=False),
        )
        .set_global_opts(
            # title_opts=opts.TitleOpts(title="动态数据"),
            xaxis_opts=opts.AxisOpts(type_="value"),
            yaxis_opts=opts.AxisOpts(type_="value"),

        )
    )
    # return line
    return line.dump_options_with_quotes()
    #c = line_base(request.form['page'])
    # print(c.dump_options_with_quotes)
    # return c.dump_options_with_quotes()

    #flask只做后端，前端用usb service启动的apache
"""
@app.route("/templates")
def index():
    return render_template("main.html")
"""

@app.after_request
def cors(environ):
    environ.headers['Access-Control-Allow-Origin'] = '*'
    environ.headers['Access-Control-Allow-Method'] = '*'
    environ.headers[
        'Access-Control-Allow-Headers'] = 'x-requested-with,content-type'
    return environ

#######################################################################################


queue = Queue()   

if __name__ == "__main__":
    
    
    #启动一个lora数据通信的mqtt线程，拿到数据，吧数据放到缓冲区，之后网页请求数据的时侯，flask后台 读该缓冲区的数据，返回给网页
    Datethread1 = wleLoRaThread(7,'0080e11500004dca')
    Datethread1.start()

    
    #启动一个wifi数据通信的线程，前端网页请求数据，flask后台发送queue队列到wifi数据通信的线程，然后tcp通信拿到数据，之后吧数据放到缓冲区里面
    #queue = Queue()
    wifi3080DateGatethread2 = wifi3080thread(queue,dateToallen=7)
    wifi3080DateGatethread2.start()
    
   #启动flask后台服务，与前端网页数据交互
    # app.run()
    server = pywsgi.WSGIServer(("localhost", 5000), app)
    server.serve_forever()

   