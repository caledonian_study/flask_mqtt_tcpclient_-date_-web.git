#!/usr/bin/python
import time
import threading
import socket
import random
import json
import re
from queue import Queue
import sys

from functools import reduce

HOST ='192.168.16.204'
#HOST ="localhost"  
PORT = 8080


wifi3080ADC={}
wifi3080ADC["wifi3080sensor"]=[0,1,2,3,4,5,6,7,8,9]
wifi3080ADC["wifi3080battery"]=[0,1,2,3,4,5,6,7,8,9]
wifi3080ADC["wifi3080chip"]=[0,1,2,3,4,5,6,7,8,9]
wifi3080ADC["wifi3080led"]=[0,1,2,3,4,5,6,7,8,9]

class wifi3080thread(threading.Thread):
    def __init__(self,queue,dateToallen=7):
        threading.Thread.__init__(self)
        self.wifiqueue = queue
        self.Tempdatelen=0
        self.Alldatelen=dateToallen

    def creatNewclient(self):
        #这里有一个Bug 一旦TCPService连接失败，将导致不好的后果，所以这里应该循环连接，
        try:
            socket.setdefaulttimeout(5)
            self.client = socket.socket(socket.AF_INET, socket.SOCK_STREAM, 0)
            self.client.connect((HOST, PORT))
        except Exception as e:
                    print("*******************"+"tcp client failed")

    def delateOldclient(self):
        try:
            self.client.close()#断开连接
        except Exception as e:
                    print("*******************"+"tcp delateOldclient failed")

    def run(self):
        print("开始线程：" + self.name)
        self.creatNewclient()

        while True:
                #队列里面阻塞要数据，然后拿数据
                val = self.wifiqueue.get()
                print(val);
                self.client.send(val)
                try:
                    print(">>Have Send Date to wifi3080")
                    self.client.settimeout(2)
                    tempdata = self.client.recv(1024).decode()
                    self.processdate(tempdata)          
                except Exception as e:
                    print(">>timeout")
                    self.creatNewclient()#断开重新连接

        self.client.close()

    def processdate(self, data):
        templist=[1,2,3,4,5,6]

        if(data != None):
            print("-------> RxData: "+data)
            mydatalist=re.findall(r"\d+\.?\d*",data)
            #print(mydatalist)
            #print(mydatalist[1])
            mydatalist=list(map(int,mydatalist))
            #print(mydatalist)

            #print(wifi3080ADC["wifi3080sensor"])

            self.Tempdatelen = self.Tempdatelen+1
            wifi3080ADC["wifi3080sensor"].append(mydatalist[1])
            
            #保证数据得长度小于预定值
            self.Tempdatelen=len(wifi3080ADC["wifi3080sensor"])
            while  self.Tempdatelen > self.Alldatelen:
                wifi3080ADC["wifi3080sensor"].pop(0)
                self.Tempdatelen=len(wifi3080ADC["wifi3080sensor"])
        
if __name__ == "__main__":
    Datethread1 = wifi3080thread(queue = Queue())
    Datethread1.start()

    
