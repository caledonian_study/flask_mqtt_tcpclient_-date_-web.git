#!/usr/bin/python
import time
import paho.mqtt.client as mqtt
import json
import base64
import matplotlib.pyplot as plt
import numpy as np
import numpy as np
import matplotlib.pyplot as plt
from matplotlib.animation import FuncAnimation
import threading
import sys
import struct
sys.path.append('d:\HaikeChallage\python_mqtt_lora_test-ok\datemqtt\webserver\dateprocess')
import random
import struct
n = {}
n1 = {}
n2 = {}

number = 0

n["acceleration.x"] =[0,1,2,3,4,5,6,7,8,9]
n["acceleration.y"] =[0,1,2,3,4,5,6,7,8,9]
n["acceleration.z"] =[0,1,2,3,4,5,6,7,8,9]

n["angular_velocity.x"]=[0,1,2,3,4,5,6,7,8,9]
n["angular_velocity.y"]=[0,1,2,3,4,5,6,7,8,9]
n["angular_velocity.z"] =[1,2,3,4,5,6,7,8,9]

n["magnetic_field.x"] =[0,1,2,3,4,5,6,7,8,9]
n["magnetic_field.y"] =[0,1,2,3,4,5,6,7,8,9]
n["magnetic_field.z"] =[0,1,2,3,4,5,6,7,8,9]

n["AppLedStateOn"] =[0,1,2,3,4,5,6,7,8,9]
n["pressure"] =[0,1,2,3,4,5,6,7,8,9]
n["temperature"] =[0,1,2,3,4,5,6,7,8,9]
n["humidity"] =[0,1,2,3,4,5,6,7,8,9]


# myplot=[]
# mysubplot=[]

# 解决中文显示问题
plt.rcParams['font.sans-serif'] = ['SimHei']
plt.rcParams['axes.unicode_minus'] = False


class wleLoRaThread(threading.Thread):
    def __init__(self, dateToallen=7, devEUI='0080e11500004dca'):
        threading.Thread.__init__(self)
        
        self.Tempdatelen=0
        self.Alldatelen=dateToallen

        self.devEUI = devEUI
        self.temp = {}
        if devEUI == '0080e11500004dca':
            self.temp = n
        elif devEUI == '0080e11500004a9c':
            self.temp = n1
        elif devEUI == '0080e1150500e525':
            self.temp = n2

    def run(self):
        print("开始线程：" + self.name)
        client = mqtt.Client(protocol=3)
        client.username_pw_set("126", "9c779be5-7f49-4e58-b46f-b216c462f651")
        client.on_connect = self.on_connect
        client.on_message = self.on_message
        client.connect(host="lorawan.timeddd.com", port=1883,
                       keepalive=60)  # 订阅频道
        time.sleep(1)
        # client.subscribe("public")
        client.subscribe([("application/126/node/"+self.devEUI+"/rx", 0)])
        client.loop_forever()

    def on_connect(self, client, userdata, flags, rc):
        if rc == 0:
            print(">>>>>>>>>>>>>>>>>>>"+self.devEUI+"连接成功"+"Connected with result code " + str(rc))


    def on_message(self, client, userdata, msg):
        self.msgget(msg)

    def msgget(self, msg):

        tempdate = []
        #print(msg.topic + " " + str(msg.payload))
        text = json.loads(msg.payload)
        # print(text)
        #print('设备的devEUI ----->', text["devEUI"])
        #print('收到的原始数据是 -->',text["deviceName"])
        # 需要转成2进制格式才可以转换，所以我们这里再手动转换一下
        result = base64.b64decode(text["data"])
        # 打印转换后的结果
        #print('收到的数据是 ------>',result)
        #print("%s: %s" % (result, time.ctime(time.time())))
        # print(result)
        devEUI = text["devEUI"]

        for ii, i in enumerate(result):
            tempdate.append(i)
        self.datepress(tempdate)
        #threadLock.release()
    def MydateToint(self,a,b,c,d):
            i=0
            temkn=[0,1,2,3]
            temkn[0]=a
            temkn[1]=b
            temkn[2]=c
            temkn[3]=d

            x = struct.unpack('<i', bytes(temkn)[:4])
            return str(x[0])
            #print(str(x[0]))


    def datepress(self, tempdate):
        global n
        
        self.Tempdatelen=len(n["acceleration.x"])
        #print("self.Tempdatelen:"+str(self.Tempdatelen))
        #print("self.Alldatelen:"+str(self.Alldatelen))
        while self.Tempdatelen > self.Alldatelen:
            #print("while")
            self.Tempdatelen=len(n["acceleration.x"])
            n["acceleration.x"].pop(0)
            n["acceleration.y"].pop(0)
            n["acceleration.z"].pop(0)

            n["angular_velocity.x"].pop(0)
            n["angular_velocity.y"].pop(0)
            n["angular_velocity.z"].pop(0)

            n["magnetic_field.x"].pop(0)
            n["magnetic_field.y"].pop(0)
            n["magnetic_field.z"].pop(0)

            n["AppLedStateOn"].pop(0)
            n["pressure"].pop(0)
            n["temperature"].pop(0)

            n["humidity"].pop(0)

        #print("datepress")
        #print(tempdate)  # 原始数据
        i=0


        n["acceleration.x"].append(self.MydateToint(tempdate[i+0],tempdate[i+1],tempdate[i+2],tempdate[i+3]))
        i=i+4
        n["acceleration.y"].append(self.MydateToint(tempdate[i+0],tempdate[i+1],tempdate[i+2],tempdate[i+3]))
        i=i+4
        n["acceleration.z"].append(self.MydateToint(tempdate[i+0],tempdate[i+1],tempdate[i+2],tempdate[i+3]))

        i=i+4
        n["angular_velocity.x"].append(self.MydateToint(tempdate[i+0],tempdate[i+1],tempdate[i+2],tempdate[i+3]))
        i=i+4
        n["angular_velocity.y"].append(self.MydateToint(tempdate[i+0],tempdate[i+1],tempdate[i+2],tempdate[i+3]))
        i=i+4
        n["angular_velocity.z"].append(self.MydateToint(tempdate[i+0],tempdate[i+1],tempdate[i+2],tempdate[i+3]))

        i=i+4
        n["magnetic_field.x"].append(self.MydateToint(tempdate[i+0],tempdate[i+1],tempdate[i+2],tempdate[i+3]))
        i=i+4
        n["magnetic_field.y"].append(self.MydateToint(tempdate[i+0],tempdate[i+1],tempdate[i+2],tempdate[i+3]))
        i=i+4
        n["magnetic_field.z"].append(self.MydateToint(tempdate[i+0],tempdate[i+1],tempdate[i+2],tempdate[i+3]))
        i=i+4       
        n["AppLedStateOn"].append(tempdate[i+0])
        n["pressure"].append(((tempdate[i+1] << 8)+tempdate[i+2])/10)
        n["temperature"].append(tempdate[i+3])
        n["humidity"].append((tempdate[i+4] << 8)+tempdate[i+5])
       

        self.Tempdatelen = self.Tempdatelen+1


        #print(n["acceleration.x"])
        #print(n["acceleration.y"])
        #print(n["acceleration.z"])


if __name__ == "__main__":

    #mydate.dateCreate()  # 创建数据库
    #threadLock = threading.Lock()
    # 创建新线程
    Datethread1 = wleLoRaThread(dateToallen=7, devEUI='0080e11500004dca')
    Datethread1.start()
